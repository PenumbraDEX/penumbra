<div align="center"> 
  <img src="https://gitlab.com/PenumbraDEX/penumbra/-/blob/master/pen.png" alt="Penumbra logo">
</div>

## Introcucing Penumbra. A truly uncensorable DEX.

Penumbra is a <a href="https://github.com/haveno-dex/haveno">Haveno's fork</a> that is more strongly aligned with the values of decentralisation, privacy and censorship resistance.

Main differences with Haveno:

- Penumbra will be accessible to users by simply using **Tor Browser**. No extra software will be required. Easy access for everyone!
- Using **2/2 Monero multisig** and **no arbitration** whatsover, Penumbra guarantees the most uncensorable trades.
- **Anyone** will be able to join as a **network operator and profit from fees**. No separate gatekeeper entity will exist. This makes Penumbra more **lucrative** to people wanting to join the network as node operators and also increases decentralisation and resistance to censorship.
- **No data** will be **stored** on the user device, but will 
- Developed **solely on donations**. **0%** of tx fee will be kept as dev fee.


## Made by the community for the community

People who want to contribute donations, code or ideas can do so on the following channels.

General: [#penumbra](https:/matrix.to/#/#penumbra:penumbra.social)
Development:[#penumbra-dex](https://matrix.to/#/#penumbra-dev:penumbra.social)

`42sjRNZYxcyWK3Bd3e6MNaR8zmjNrze8W5fDjttJ152WPReFUj5ung4fw7y73DTtFXjVRGSkonjW5J5XvUXub2xEV3ufoK4`

![Qr code](https://gitlab.com/PenumbraDEX/penumbra/-/blob/master/monero.gif)

## Current status

PoC coming soon!


